const Joi = require('joi');
const SchemaModel = require('../config/schema');

const optionalParams = ['title', 'description', 'price', 'imageURL','retailPrice','collectionId'];
const productSchema = {
    hashKey: 'collectionType',
    rangeKey: 'collectionId',
    timestamps: true,
    schema: Joi.object({
        collectionType: 'product', //Product
        collectionId: Joi.string().alphanum(), //ProductId
        title: Joi.string(),
        description: Joi.string(),
        price: Joi.number().positive().min(1),
        imageURL: Joi.string(),
        retailPrice:Joi.number().positive().min(1)
    }).optionalKeys(optionalParams).unknown(true)
}


const attToGet = ['title', 'description', 'price', 'imageURL','retailPrice','collectionId'];
const attToQuery = ['description', 'imageURL', 'price', 'size', 'title', 'collectionId'];
const optionsObj = {
    attToGet,
    attToQuery,
};
const Product = SchemaModel(productSchema, optionsObj);

module.exports = Product;