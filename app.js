const express = require('express');
const path = require('path');
const logger = require('morgan');
const cookieParser = require('cookie-parser');
const bodyParser = require('body-parser');
var session = require('express-session');
const methodOverride = require('method-override');
const config = require('./config/config');
const DynamoDBStore = require('dynamodb-store');
const jwt = require('jsonwebtoken');
const subdomain= require('express-subdomain');

const app = express();
const router = express.Router();


app.use(logger('dev'));
app.use(bodyParser.json());
app.use(bodyParser.urlencoded({extended: true}));
app.use(cookieParser());

// view engine setup
app.set('views', path.join(__dirname, 'views'));
app.set('view engine', 'ejs');   
//Flash messages
//app.use(flash()); 

app.use(express.static(path.join(__dirname, 'public')));
app.use(methodOverride('_method'));



//Middleware
app.use('/', router);  
router.use((req, res, next)=> {
    res.setHeader('Access-Control-Allow-Origin', '*');
    res.setHeader('Access-Control-Allow-Methods', 'GET, POST, PUT, DELETE');
    next();
});

//Route variables
const users= require('./routes/user/api');

//Routing
app.use('/',users);
module.exports = app;
